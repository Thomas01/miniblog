import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from './Store'
import { StateData } from '../types'

const initialState: StateData = {
    loading: false,
    hasErrors: false,
    listing:[],
}

export const listingsSlice = createSlice({
  name: 'listings',
  initialState,
  reducers: {
    getList: state => {
      state.loading = true
    },
    getListSuccess: (state, { payload }) => {
      state.listing = payload
      state.loading = false
      state.hasErrors = false
    },
    getListFailure: state => {
      state.loading = false
      state.hasErrors = true
    },
  },
})

export const { getList, getListSuccess, getListFailure } = listingsSlice.actions
export const selectListings = (state: RootState) => state
export default listingsSlice.reducer

