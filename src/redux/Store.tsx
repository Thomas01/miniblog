import { configureStore } from '@reduxjs/toolkit';
import { listingsSlice } from '../redux/listingsSlice';

export const store = configureStore({
  reducer: {
   listing: listingsSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>; 
export type AppDispatch = typeof store.dispatch