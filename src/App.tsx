import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
//import './App.css'
import CardUi from './components/Card';
import Listings from "./components/Listings";
import NavBar from './components/NavBar';

const App = () => {

  return (
    <BrowserRouter>
    <NavBar />
    <Routes>
      <Route path="/" element={<Listings />} />
      <Route path="list/:display_name" element={<CardUi /> } />
    </Routes>
  </BrowserRouter>
  )
}

export default App
