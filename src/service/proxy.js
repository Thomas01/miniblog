const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = (app) => {
  app.use(
    createProxyMiddleware("/endpoint", {
      target: "https://www.reddit.com/api/v1/",
      changeOrigin: true,
    })
  );
};
