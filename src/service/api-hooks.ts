import { AnyAction } from 'redux';
import axios from 'axios';
import { ThunkAction } from 'redux-thunk'
import type { RootState, AppDispatch } from '../redux/Store'
import { useDispatch } from 'react-redux';
import { getList, getListSuccess, getListFailure} from '../redux/listingsSlice';
import {PostData} from "../types"

export const useAppDispatch = () => useDispatch<AppDispatch>()
export function fetchListings() : ThunkAction<void, RootState, unknown, AnyAction> {
   return async dispatch => {
     dispatch(getList())
     let baseUrl ="https://www.reddit.com/subreddits/search.json?q=trending_subreddits"
     try {
       const {data:{data:{children}}} = await axios.get(baseUrl );
       const modifieldData = children.map((res:PostData) => ({
         display_name: res.data.display_name,
         id: res.data.id,
         title: res.data.title,
         url: res.data.url,
         public_description: res.data.public_description
       }))
       dispatch(getListSuccess(modifieldData))
     } catch (error) {
       dispatch(getListFailure())
     }
   }
 }