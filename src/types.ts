export type PostData = {
  id: number | string;
  title: string;
  data:  any,
  author: string,
  code:string,
  url: string,
  public_description: string,
  display_name: string,
};

export interface StateData {
  loading: boolean,
  hasErrors: boolean,
  listing: []
}
 
