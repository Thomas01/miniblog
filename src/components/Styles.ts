import styled from "styled-components";

export const CardWrapper = styled.div`
  overflow: hidden;
  padding: 0 0 20px;
  margin: 10px auto 0;
  height: 31rem;
  font-family: Quicksand, arial, sans-serif;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.05), 0 0px 40px rgba(0, 0, 0, 0.08);
  border-radius: 5px;
  cursor: pointer;
  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const BodyWrapper = styled.div`
margin-top: 30px;
`;

export const CardHeader = styled.header`
  margin-top: 3rem;
  padding-top: 20px;
  padding-bottom: 7px;
  display: flex;
  justify-content: center;
  width: 100%;
  flex-wrap: wrap;
  float: right;
`;

export const CardHeading = styled.h1`
  font-size: 0.85rem;
  color: #666666;
  margin-top: -2.2rem;
  overflow: hidden;
  padding: 0 2rem;
`;

export const CardHead = styled.h2`
  margin-left: 5rem;
  font-size: 16px;
  font-weight: 550;
  color: #666666;
  @media (max-width: 768px) {
    width: 100%;
    margin-left: 1rem;
    font-size: 15px;
  }
`;
export const Paragraphy = styled.p`
  text-align: center;
  margin-top: 10%;
  color: red;
  font-size: 25px;
`;

export const CardBody = styled.div`
  margin-top: 3.5rem;
  padding: 0 2rem 0 2rem;
`;

export const CardField = styled.fieldset`
  display: flex;
  position: relative;
  padding: 0;
  margin: 0;
  border: 0;
`;

export const MainParagraphy = styled.p`
  margin-top: 10px;
  font-size: 0.85rem;
  margin-left: 5rem;
  &::before {
    content: "➡ ";
    color: #222222;
    font-size: 20px;
  }
`;

export const linkStyle = {
  textDecoration: "none",
  color: '#444'
};

export const CardNote = styled.small`
  padding: 0 2rem;
  width: 100%;
  font-size: 0.89rem;
  color: #333;
  text-align: center;
`;

export const CardButton = styled.button`
  display: block;
  width: 10%;
  height: 20px;
  padding: 2px 0;
  font-family: inherit;
  font-size: 14px;
  font-weight: 700;
  color: #fff;
  margin: 2rem 0 0 4.5rem;
  z-index: 99;
  background-color: #e5195f;
  border: 0;
  border-radius: 35px;
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.08);
  cursor: pointer;
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);
  &:hover {
    box-shadow: 0 15px 15px rgba(0, 0, 0, 0.16);
    transform: translate(0, -5px);
  }
  @media (max-width: 768px) {
    font-size: 12px;
    width: 20%;
  }
`;

// Listings component styles
export const Container = styled.div`
  width: 90%;
  margin: 30px auto;
`;




