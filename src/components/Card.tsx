import { useEffect, useState } from 'react'
import {
  CardWrapper,
  CardHeader,
  CardHeading,
  CardBody,
  CardField,
  CardNote,
  Paragraphy,
  CardButton,
  BodyWrapper,
  linkStyle} from "./Styles"
  import {useParams} from "react-router-dom"
  import { PostData } from '../types'
  import { Link } from "react-router-dom";
  import { useAppSelector } from '../redux/Hooks';

  const CardUi = () => {
  const list = useAppSelector((state) => state.listing);
  const [card, setCard] = useState<Array<PostData>>([])
  const{loading , listing} = list
  let { display_name } = useParams();
 
  const fetchFilteredList = () => {
    return listing.filter(
      (post:PostData) => post.display_name === display_name
    );
  };

  //Avoid repetitive function calls
  useEffect(() => {
  let CardData = fetchFilteredList()
  let cards = CardData as Array<PostData>
  setCard(cards)
  }, [])

  return (
    <BodyWrapper>
        {loading ? <Paragraphy>Fetching post...</Paragraphy> : (
         card.map((post:PostData)=>(
          <CardWrapper key={post.id}>
          <Link to="/" style={linkStyle}>
          <CardButton>
             Back
          </CardButton>
          </Link>
          <CardHeader>
            <CardHeading>{post.title}</CardHeading>
          </CardHeader>
            <CardBody>
              <CardField>
              <CardNote>{post.public_description}</CardNote>
              </CardField>
            </CardBody>
        </CardWrapper>
        )))}
    </BodyWrapper>
  )
}
export default CardUi