import {useEffect} from 'react'
import {
    Container,
    Paragraphy,
    MainParagraphy,
    linkStyle,
    CardHead  
} from "./Styles"
import { PostData} from '../types';
import { Link } from "react-router-dom";
import { useAppDispatch , useAppSelector } from '../redux/Hooks';
import { fetchListings } from '../service/api-hooks'; 

const Listings = () =>  {
  const dispatch = useAppDispatch()
  const list = useAppSelector((state) => state.listing);
  const{hasErrors, loading , listing} = list
  
  useEffect(() => {
  dispatch(fetchListings())
  },[dispatch])

  return (
    <Container>
        {hasErrors && (<Paragraphy>Oops! Something went wrong. Please try again or check your network connection.</Paragraphy> )}
        {!loading && !hasErrors ? <CardHead>Click on any of listings below to read more</CardHead>: ""}
        {loading ? <Paragraphy>Fetching posts...</Paragraphy> : (
         listing.map((post:PostData)=>(
          <MainParagraphy key={post.id}>
            <Link to={`/list/${post.display_name}`} style={linkStyle}>
            {post.title}
            </Link>
          </MainParagraphy>
        )))}   
    </Container>
    )
}
export default Listings