import styled from 'styled-components';

const NavBar = () => {

 const Nav = styled.nav`
  background: #ffffff;
  height: 60px;
  border-bottom: 1px solid #999999;
  position: sticky;
  top: 0;
  width: 100%;
`;
  return (
    <Nav></Nav>
  )
}

export default NavBar